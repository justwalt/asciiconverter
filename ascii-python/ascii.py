"""
Converts an image to an ascii representation of that image and prints it to the
terminal.

"""


import cv2
import numpy as np
from sys import argv
from os import popen
import colored
import multiprocessing
from math import floor

import pyximport

pyximport.install(language_level=3)
from libascii import cy_nearest_color


def main():
    image_location, quality = argument_handler()

    image = cv2.imread(image_location)

    # Scale the image with OpenCV so we don't have to.
    dim_x, dim_y = choose_dimensions(image.shape)
    image = cv2.resize(image, (dim_x, dim_y))

    num_rows = image.shape[0]
    num_threads = multiprocessing.cpu_count()
    divisor = num_rows / num_threads

    manager = multiprocessing.Manager()
    return_dict = manager.dict()

    thread_group = []
    for i in range(num_threads):
        lower = floor(i * divisor)
        upper = floor((i + 1) * divisor)
        morsel = image[lower:upper, :]
        thread_group.append(
            multiprocessing.Process(
                target=convert_ascii, args=(morsel, quality, i, return_dict)
            )
        )

    for thread in thread_group:
        thread.start()

    for thread in thread_group:
        thread.join()

    for i in range(num_threads):
        for row in return_dict[i]:
            for pixel in row:
                print(pixel, end="")
            print("")


def convert_ascii(image_chunk, quality, index, return_dict):
    ascii_out = []
    for pixel_row in image_chunk:
        ascii_row = []
        for pixel in pixel_row:
            ascii_pixel = color_gradientize(pixel, quality)
            ascii_row.append(ascii_pixel)
        ascii_out.append(ascii_row)

    return_dict[index] = ascii_out


def argument_handler():
    try:
        image_location = argv[1]
    except IndexError:
        print("No image given. Exiting.")
        raise Exception

    try:
        quality = argv[2]
    except IndexError:
        print("Defaulting to low quality. Options are 0-7.")
        quality = 16

    if quality == "0":
        # print("Using 16 colors.")
        quality = 16
    elif quality == "1":
        # print("Using 52 colors.")
        quality = 52
    elif quality == "2":
        # print("Using 88 colors.")
        quality = 88
    elif quality == "3":
        # print("Using 124 colors.")
        quality = 124
    elif quality == "4":
        # print("Using 160 colors.")
        quality = 160
    elif quality == "5":
        # print("Using 196 colors.")
        quality = 196
    elif quality == "6":
        # print("Using 232 colors.")
        quality = 232
    elif quality == "7":
        # print("Using 256 colors.")
        quality = 256
    else:
        print("Invalid quality given, defaulting to max. Options are 0-7")
        quality = 256

    return (image_location, quality)


def color_gradientize(pixel, quality) -> str:
    # Convert a pixel to a terminal colored string
    gray_pixel = bw_gradientize(pixel)

    color = cy_nearest_color(pixel, quality)

    return colored.fg(color) + gray_pixel + colored.attr("reset")


def bw_gradientize(pixel) -> str:
    # Convert the pixel to a luminance value (bgr -> grayscale)
    gray_pixel = 0.21 * pixel[2] + 0.72 * pixel[1] + 0.07 * pixel[0]
    # gray_pixel = (pixel[0] + pixel[1] + pixel[2]) / 3
    # gray_pixel = (
    #     max(pixel[0], max(pixel[1], [pixel[2]]))
    #     + min(pixel[0], min(pixel[1], [pixel[2]]))
    # ) / 2

    # gradient = np.array([".", ",", "o", "*", "0", "#", "@"])
    gradient = np.array([" ", "░", "▒", "▓", "█"])
    thresholds = [1, 20, 70, 190, 255]

    for i in range(len(gradient)):
        if gray_pixel <= thresholds[i]:
            return gradient[i]
        else:
            continue

    # if no match, return last val
    return gradient[-1]


def choose_dimensions(orig_dims) -> (int, int):
    # Decides a good set of dimensions for the output.

    # Get the terminal size for comparison.
    dim_y, dim_x = popen("stty size", "r").read().split()
    dim_x = int(dim_x)
    dim_y = int(dim_y)

    og_dim_x = orig_dims[1] * 2  # *2 to make the aspect ratio more appealing
    og_dim_y = orig_dims[0]

    x_scale = og_dim_x / dim_x
    y_scale = og_dim_y / dim_y
    scale_factor = x_scale if x_scale > y_scale else y_scale

    out_x = int(og_dim_x / scale_factor)
    out_y = int(og_dim_y / scale_factor)

    return (out_x, out_y)


if __name__ == "__main__":
    main()
