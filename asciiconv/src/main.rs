extern crate image;
extern crate libascii;
extern crate terminal_size;

use std::env;

use terminal_size::{terminal_size, Height, Width};

use libascii::display_image_as_ascii;
use libascii::load_image_as_rgb;
use libascii::resize_image_for_terminal;

fn main() {
    let args: Vec<String> = env::args().collect();

    if args.len() != 2 {
        println!("Missing filepath argument!");
        return;
    }

    let image_location = args[1].to_string();
    let image_rgb = load_image_as_rgb(image_location);

    let image_width = image_rgb.dimensions().0;
    let image_height = image_rgb.dimensions().1;
    let (term_width, term_height) = get_terminal_size();
    let (dim_width, dim_height) =
        choose_dims_with_aspect_ratio(term_width, term_height, image_width, image_height);
    let scaled_image = resize_image_for_terminal(image_rgb, dim_width, dim_height);

    display_image_as_ascii(scaled_image);
}

fn get_terminal_size() -> (u32, u32) {
    let term_size = terminal_size();

    if let Some((Width(w), Height(h))) = term_size {
        return (w.into(), h.into());
    } else {
        panic!("Couldn't get terminal size.");
    }
}

fn choose_dims_with_aspect_ratio(
    term_width: u32,
    term_height: u32,
    image_width: u32,
    image_height: u32,
) -> (u32, u32) {
    // This function needs to pick some sensible dimensions for the image to be printed.
    let scaled_image_width = image_width * 2;

    let w_scale = (scaled_image_width as f32) / (term_width as f32);
    let h_scale = (image_height as f32) / (term_height as f32);

    let scale_factor = if w_scale > h_scale { w_scale } else { h_scale };

    let new_dim_width = ((scaled_image_width as f32) / scale_factor).floor() as u32;
    let new_dim_height = ((image_height as f32) / scale_factor).floor() as u32;

    (new_dim_width, new_dim_height)
}
